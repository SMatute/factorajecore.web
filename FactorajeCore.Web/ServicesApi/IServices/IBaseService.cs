﻿using FactorajeCore.Web.Models;

namespace FactorajeCore.Web.ServicesApi.IServices
{
    public interface IBaseService
    {
        APIResponse responseModel { get; set; }
        Task<T> SendAsync<T>(APIRequest apiRequest);
        T Send<T>(APIRequest apiRequest);

    }
}
