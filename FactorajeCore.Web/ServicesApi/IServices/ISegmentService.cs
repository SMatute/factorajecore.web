﻿namespace FactorajeCore.Web.ServicesApi.IServices
{
    public interface ISegmentService
    {
        Task<T> GetAllAsync<T>();
        Task<T> GetByIdAsync<T>(int id);
    }
}
