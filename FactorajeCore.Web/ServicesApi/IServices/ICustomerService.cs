﻿
using FactorajeCore.Web.Models.Dtos;

namespace FactorajeCore.Web.ServicesApi.IServices
{
    public interface ICustomerService
    {
        Task<T> GetAllAsync<T>();
        T GetAll<T>();
        Task<T> GetByCodeAsync<T>(string code);
        Task<T> CreateAsync<T>(CustomerDtoCreate dtoCreate);
        Task<T> UpdateAsync<T>(string code, CustomerDtoUpdate dtoUpdate);
        Task<T> DeleteAsync<T>(string code);
    }
}
