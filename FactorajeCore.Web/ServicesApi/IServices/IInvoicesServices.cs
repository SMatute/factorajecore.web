﻿using FactorajeCore.Web.Models.Dtos;

namespace FactorajeCore.Web.ServicesApi.IServices;

public interface IInvoicesServices
{
    Task<T> GetAllAsync<T>();

    Task<T> GetByCodeAsync<T>(int registryid);
    Task<T> CreateAsync<T>(InvoicesDtoCreate dtoCreate);
    Task<T> UpdateAsync<T>(int registryid, InvoicesDtoUpdate dtoUpdate);
    Task<T> GetByDateCustomer<T>(string fechaFac, string customerCode);
    Task<T> GetAllDateAsync<T>(string date);
    Task<T> GetAllCreatedAtAsync<T>(string date);
    Task<T> DeleteAsync<T>(int resgistryId);
}