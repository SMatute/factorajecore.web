﻿namespace FactorajeCore.Web.ServicesApi.IServices;

public interface ILetraDeCorteService
{
    Task<T> GetByDateAsync<T>(string fechaIni, string fechaFin);
}
