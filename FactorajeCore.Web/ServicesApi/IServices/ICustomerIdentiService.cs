﻿namespace FactorajeCore.Web.ServicesApi.IServices
{
    public interface ICustomerIdentiService
    {
        Task<T> GetAllAsync<T>();
        Task<T> GetByIdAsync<T>(int Id);
    }
}
