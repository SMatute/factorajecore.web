﻿namespace FactorajeCore.Web.ServicesApi.IServices;

public interface ICollectionAgentService
{
    Task<T> GetAllAsync<T>();
    Task<T> GetByIdAsync<T>(int Id);
}
