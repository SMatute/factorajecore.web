﻿using FactorajeCore.Web.Models.Dtos;

namespace FactorajeCore.Web.ServicesApi.IServices
{
    public interface ICurrencyService
    {
        Task<T> GetAllAsync<T>();
        Task<T> GetByCodeAsync<T>(string code);
        Task<T> UpdateAsync<T>(string code,CurrenciesDtoUpdate dto);

    }
}
