﻿using FactorajeCore.Web.Models.Dtos;

namespace FactorajeCore.Web.ServicesApi.IServices;

public interface IExchangeRateServices
{
    Task<T> GetAllAsync<T>(string code);
    Task<T> GetByCodeAsync<T>(DateTime dateTrx, string code);
    Task<T> CreateAsync<T>(ExchangeRateDtoCreate dtoCreate);
    Task<T> UpdateAsync<T>(string code, string date, ExchangeRateDtoUpdate dtoUpdate);
    Task<T> DeleteAsync<T>(string code);
}
