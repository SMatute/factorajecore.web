﻿using FactorajeCore.Web.ServicesApi.IServices;

namespace FactorajeCore.Web.ServicesApi.Services;

public class SegmentService: BaseService, ISegmentService
{

    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/Segment";
    private readonly string endpointByIdUrl = "/GetById";
    public string baseUrl;
    public SegmentService(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }

    public Task<T> GetAllAsync<T>()
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl
        });
    }

    public Task<T> GetByIdAsync<T>(int id)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByIdUrl + "?id=" + id
        });
    }

}

