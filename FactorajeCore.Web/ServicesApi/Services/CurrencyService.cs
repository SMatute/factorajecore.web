﻿using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.ServicesApi.IServices;

namespace FactorajeCore.Web.ServicesApi.Services
{
    public class CurrencyService : BaseService, ICurrencyService
    {
        public IHttpClientFactory _clientFactory { get; }
        private readonly string endpointUrl = "/api/FactorajeCurrencies";
        private readonly string endpointByCodeUrl = "/GetByCode";

        public string baseUrl;

        public CurrencyService(IHttpClientFactory clientFactory, IConfiguration configuration):base(clientFactory)
        {
            _clientFactory = clientFactory;
            baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
        }

        public Task<T> GetAllAsync<T>()
        {
            return SendAsync<T>(new Models.APIRequest()
            {
                ApiType = Util.SD.ApiType.GET,
                Url = baseUrl + endpointUrl
            });
        }

        public Task<T> GetByCodeAsync<T>(string code)
        {
            return SendAsync<T>(new Models.APIRequest()
            {
                ApiType = Util.SD.ApiType.GET,
                Url = baseUrl + endpointUrl + endpointByCodeUrl+"?code="+ code.Trim()
            });
        }

        public Task<T> UpdateAsync<T>(string code,CurrenciesDtoUpdate dto)
        {
            return SendAsync<T>(new Models.APIRequest()
            {
                ApiType = Util.SD.ApiType.PUT,
                Data = dto,
                Url = baseUrl + endpointUrl + "?code=" + code.Trim()
            });
        }


    }
}
