﻿using FactorajeCore.Web.ServicesApi.IServices;

namespace FactorajeCore.Web.ServicesApi.Services;

public class LetraDeCorteService: BaseService, ILetraDeCorteService
{
    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/LetraDeCorte";
    public string baseUrl;
    public LetraDeCorteService(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }

    public Task<T> GetByDateAsync<T>(string fechaIni, string fechaFin)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + "?dateinit=" + fechaIni + "&datefin=" + fechaFin
        });
    }
}
