﻿using FactorajeCore.Util;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.Models;
using FactorajeCore.Web.ServicesApi.IServices;

namespace FactorajeCore.Web.ServicesApi.Services;

public class ExchangeRateServices: BaseService, IExchangeRateServices
{
    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/SerLatCRExchangeRate";
    private readonly string deleteUrl = "/api/SerLatCRExchangeRate/";
    private readonly string endpointByCodeUrl = "/GetByCode";
    private readonly string endpointByDateUrl = "/GetByByDate";
    public string baseUrl;

    public ExchangeRateServices(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }

    public Task<T> GetAllAsync<T>(string code)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + "?code=" + code
        });
    }
   

    public Task<T> GetByCodeAsync<T>(DateTime dateTrx, string codeCurrency )
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByDateUrl + "?dateTransa=" + dateTrx + "&currencyCode=" + codeCurrency
        });
    }
    public Task<T> CreateAsync<T>(ExchangeRateDtoCreate dtoCreate)
    {
        return SendAsync<T>(new APIRequest()
        {
            ApiType = SD.ApiType.POST,
            Data = dtoCreate,
            Url = baseUrl + endpointUrl
        });
    }

    public Task<T> UpdateAsync<T>(string code, string date, ExchangeRateDtoUpdate dtoUpdate)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.PUT,
            Data = dtoUpdate,
            Url = baseUrl + endpointUrl + "?datetranx=" + date
        });
    }
    public Task<T> DeleteAsync<T>(string code)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = SD.ApiType.DELETE,
            Url = baseUrl + deleteUrl + code,

        });
    }
}
