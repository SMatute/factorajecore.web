﻿
using FactorajeCore.Util;
using FactorajeCore.Web.Models;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.ServicesApi.IServices;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace FactorajeCore.Web.ServicesApi.Services;

public class CustomerService: BaseService, ICustomerService
{
    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/Customer";
    private readonly string deleteUrl = "/api/Customer/";
    private readonly string endpointByCodeUrl = "/GetByCode";
    public string baseUrl;

    public CustomerService(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }

    public Task<T> GetAllAsync<T>()
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl
        });
    }
    public T GetAll<T>()
    {
        return Send<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl
        });
    }

    public Task<T> GetByCodeAsync<T>(string code)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByCodeUrl + "?code=" + code
        });
    }
    public Task<T> CreateAsync<T>(CustomerDtoCreate dtoCreate)
    {
        return SendAsync<T>(new APIRequest()
        {
            ApiType = SD.ApiType.POST,
            Data = dtoCreate,
            Url = baseUrl + endpointUrl 
        });
    }

    public Task<T> UpdateAsync<T>(string code, CustomerDtoUpdate dtoUpdate)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.PUT,
            Data = dtoUpdate,
            Url = baseUrl + endpointUrl + "?customerCode=" + code
        });
    }
    public Task<T> DeleteAsync<T>(string code)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = SD.ApiType.DELETE,
            Url = baseUrl + deleteUrl + code,
           
        });
    }


}

