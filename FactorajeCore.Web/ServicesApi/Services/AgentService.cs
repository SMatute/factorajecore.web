﻿using FactorajeCore.Web.ServicesApi.IServices;
using Microsoft.Extensions.Configuration;

namespace FactorajeCore.Web.ServicesApi.Services;

public class AgentService: BaseService, IAgentService
{
    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/SerLatCRAgent";
    private readonly string endpointByIdUrl = "/GetById";
    public string baseUrl;

    public AgentService(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }

    public Task<T> GetAllAsync<T>()
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl
        });
    }

}
