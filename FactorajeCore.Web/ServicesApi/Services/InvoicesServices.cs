﻿using FactorajeCore.Util;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.ServicesApi.IServices;

namespace FactorajeCore.Web.ServicesApi.Services;

public class InvoicesServices: BaseService, IInvoicesServices
{
    public IHttpClientFactory _clientFactory { get; }
    private readonly string endpointUrl = "/api/Invoices/";
    private readonly string endpointByIdUrl = "GetBydate";
    private readonly string endpointByDateUrl = "GetByDate";
    private readonly string endpointByCreatedAtUrl = "CreatedAt";
    public string baseUrl;
    public InvoicesServices(IHttpClientFactory clientFactory, IConfiguration configuration) : base(clientFactory)
    {
        _clientFactory = clientFactory;
        baseUrl = configuration.GetValue<string>("ServiceUrls:factorajeAPI");
    }
    public Task<T> GetAllAsync<T>()
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl
        });
    }

    public Task<T> GetAllDateAsync<T>(string date)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByDateUrl + "?dateinvoice=" + date
        });
    }

    public Task<T> GetAllCreatedAtAsync<T>(string date)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByCreatedAtUrl + "?createdAtinvoice=" + date
        });
    }
    public Task<T> GetByCodeAsync<T>(int registryid)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByIdUrl + "?registryid=" + registryid
        });
    }
    public Task<T> GetByDateCustomer<T>(string fechaFac, string customerCode)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.GET,
            Url = baseUrl + endpointUrl + endpointByIdUrl + "?fecha=" + fechaFac + "&codCliente=" + customerCode
        });
    }

    public Task<T> CreateAsync<T>(InvoicesDtoCreate dtoCreate)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = SD.ApiType.POST,
            Data = dtoCreate,
            Url = baseUrl + endpointUrl
        });
    }

    public Task<T> UpdateAsync<T>(int registryid, InvoicesDtoUpdate dtoUpdate)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = Util.SD.ApiType.PUT,
            Data = dtoUpdate,
            Url = baseUrl + endpointUrl + "?customerCode=" + registryid
        });
    }

    public Task<T> DeleteAsync<T>(int registryId)
    {
        return SendAsync<T>(new Models.APIRequest()
        {
            ApiType = SD.ApiType.DELETE,
            Url = baseUrl + endpointUrl + registryId,

        });
    }
}
