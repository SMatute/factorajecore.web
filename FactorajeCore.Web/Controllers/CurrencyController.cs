﻿using AutoMapper;
using FactorajeCore.Web.Models;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.ServicesApi.IServices;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FactorajeCore.Web.Controllers;

public class CurrencyController : Controller
{
    private readonly ICurrencyService _service;
    private readonly IMapper _mapper;
    public CurrencyController(ICurrencyService service, IMapper mapper)
    {
        _service = service; 
        _mapper = mapper;
    }
    public async Task<IActionResult> Index()
    {
        List<CurrenciesDto> list = new();

        var response = await _service.GetAllAsync<APIResponse>();
        if ((response != null) && response.IsSuccess)
        {
            list = JsonConvert.DeserializeObject<List<CurrenciesDto>>(Convert.ToString(response.Result));
        }
        return View(list);
    }

    [HttpGet]
    public async Task<IActionResult> DetailCurrency(string code)
    {
        CurrenciesDto model = new();
        var response = await _service.GetByCodeAsync<APIResponse>(code);
        if ((response != null) && response.IsSuccess)
        {
            model = JsonConvert.DeserializeObject<CurrenciesDto>(Convert.ToString(response.Result));
        }
        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> UpdateCurrency(string code)
    {
        CurrenciesDto model = new();
        var response = await _service.GetByCodeAsync<APIResponse>(code);
        if ((response != null) && response.IsSuccess)
        {
            model = JsonConvert.DeserializeObject<CurrenciesDto>(Convert.ToString(response.Result));
            return View(_mapper.Map<CurrenciesDtoUpdate>(model));
        }
        return NotFound();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateCurrency(CurrenciesDtoUpdate model)
    {
        if (ModelState.IsValid)
        {
            var response = await _service.UpdateAsync<APIResponse>(model.Code, model);
            if ((response != null) && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
        }
        return View(model);
    }


}
