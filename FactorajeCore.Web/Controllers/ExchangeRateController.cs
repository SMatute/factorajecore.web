﻿using AutoMapper;
using FactorajeCore.Web.Models;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.ServicesApi.IServices;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Configuration;
using System.Reflection;

namespace FactorajeCore.Web.Controllers;

public class ExchangeRateController : Controller
{
    private readonly IExchangeRateServices _services;
    private readonly IConfiguration _configuration;
    private readonly IMapper _mapper;
    public string codeCurrency;

    public ExchangeRateController(IExchangeRateServices services, IConfiguration configuration, IMapper mapper)
    {
        _services = services;
        this._configuration = configuration;
        _mapper = mapper;
    }
    public async Task<IActionResult> Index()
    {
        return View();
    }
    [HttpGet]
    public async Task<IActionResult> GetAllData()
    {
        codeCurrency = _configuration.GetValue<string>("ServiceUrls:CodeExchange");
        List<ExchangeRateDto> list = new();
        var response = await _services.GetAllAsync<APIResponse>(codeCurrency);
        if ((response != null) && response.IsSuccess)
        {
            list = JsonConvert.DeserializeObject<List<ExchangeRateDto>>(Convert.ToString(response.Result));
        }
        return Json(new { data = list });
    }

    [HttpPost]
   
    public async Task<IActionResult> CreateExchange(ExchangeRateDtoCreate dtoCreate)
    {
        if (ModelState.IsValid)
        {
            var response = await _services.CreateAsync<APIResponse>(dtoCreate);
            if (response != null && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (response.ErrorMessages.Count > 0)
                {
                    ModelState.AddModelError("ErrorMessages", response.ErrorMessages.FirstOrDefault());
                }
            }
        }
        return View(dtoCreate);

    }

    public async Task<IActionResult>UpdateExchange(DateTime dateTrx, string codeCurrency)
    {
        codeCurrency = _configuration.GetValue<string>("ServiceUrls:CodeExchange");
        ExchangeRateDto model = new();
        var response = await _services.GetByCodeAsync<APIResponse>(dateTrx, codeCurrency);
        if ((response != null) && response.IsSuccess)
        {
            model = JsonConvert.DeserializeObject<ExchangeRateDto>(Convert.ToString(response.Result));
            return View(_mapper.Map<CurrenciesDtoUpdate>(model));
        }
        return NotFound();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateExchange(ExchangeRateDtoUpdate model)
    {
        if (ModelState.IsValid)
        {
            var response = await _services.UpdateAsync<APIResponse>(model.currencyCode, model.dateTrx.ToString(), model);
            if ((response != null) && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
        }
        return View(model);
    }
}
