﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using FactorajeCore.Web.Models;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.Models.VM;
using FactorajeCore.Web.ServicesApi.IServices;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Reflection;
using System.Collections.Immutable;
using Microsoft.DotNet.Scaffolding.Shared.Messaging;
using System.Collections.Generic;
using Microsoft.AspNetCore.Server.IIS.Core;

namespace FactorajeCore.Web.Controllers;


public class CustomerController : Controller
{
    
    private readonly ICustomerService _service;
    private readonly ICustomerIdentiService _serviceIdentificationType;
    private readonly IAgentService _serviceAgent;
    private readonly ICollectionAgentService _serviceCollectionAgent;
    private readonly ISegmentService _servicesegment;
    private readonly IMapper _mapper;

    public CustomerController(ICustomerService service
        , ICustomerIdentiService serviceIdentificationType
        , IAgentService serviceAgent
        , ICollectionAgentService serviceCollectionAgent
        , ISegmentService segmentService
        , IMapper mapper)
    {
        _service = service;
        _serviceIdentificationType = serviceIdentificationType;
        _serviceAgent = serviceAgent;
        _serviceCollectionAgent = serviceCollectionAgent;
        this._servicesegment = segmentService;
        _mapper = mapper;
    }
    public async Task<IActionResult> Index()
    {
        //var dataList = await GetAllData();
        return View();
    }


    [HttpGet]
    public IActionResult  GetAllData()
    {
        customerDetailsVM customerDetails = new();
        List<CustomerDto> list = new();
        var task =_service.GetAllAsync<APIResponse>();
        var response = task.Result;
        if ((response != null) && response.IsSuccess)
        {
            list = JsonConvert.DeserializeObject<List<CustomerDto>>(Convert.ToString(response.Result)).ToList();
        }
        var listViewData = list.Select(x => new CustomerIndexVM
        {
            code = x.Code,
            razon = x.Razon,
            email = x.Email,
            numberidentity = x.NumberCED
        }).ToList();

        //new { data = listViewData}
        return Json(new { data = listViewData });
    }

    [HttpGet]
    public async Task<IActionResult> DetailCustomer(string code)
    {
        customerDetailsVM customerDetails = new();
        var response = await _service.GetByCodeAsync<APIResponse>(code);
        if (response != null && response.IsSuccess)
        {
            CustomerDto model = JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(response.Result));
            customerDetails.EntityDetail = model;


        }
        response = await _serviceAgent.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDetails.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
        }
        response = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDetails.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
        }

        response = await _serviceIdentificationType.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDetails.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.description,
                    Value = x.registryid.ToString()
                });
        }

        response = await _servicesegment.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDetails.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.AssignedSegment,
                    Value = x.IdSegment.ToString()
                });
            return View(customerDetails);
        }
        return NotFound();

        //var response = await _service.GetByCodeAsync<APIResponse>(code);
        //if ((response != null) && response.IsSuccess)
        //{
        //    model = JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(response.Result));
        //}
        //return View(model);
    }

   

    [HttpGet]
    public async Task<IActionResult> CreateCustomer()
    {
        var model = new CustomerCreateVM();

        var responseList = await _serviceIdentificationType.GetAllAsync<APIResponse>();

        if (responseList != null && responseList.IsSuccess)
        {
            model.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>(
                Convert.ToString(responseList.Result))
                .Select(x => new SelectListItem
                {
                    Text = x.description,
                    Value = x.registryid.ToString()
                });
        }

        responseList = await _serviceAgent.GetAllAsync<APIResponse>();
        if (responseList != null && responseList.IsSuccess)
        {
            model.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>(
                Convert.ToString(responseList.Result))
                .Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
        }

        responseList = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
        if (responseList != null && responseList.IsSuccess)
        {
            model.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>(
                Convert.ToString(responseList.Result))
                .Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
            responseList = await _servicesegment.GetAllAsync<APIResponse>();
            if (responseList != null && responseList.IsSuccess)
            {
                model.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(
                    Convert.ToString(responseList.Result))
                    .Select(x => new SelectListItem
                    {
                        Text = x.AssignedSegment,
                        Value = x.IdSegment.ToString()
                    });
            }
        }

        return View(model);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> CreateCustomer(CustomerCreateVM model)
    {

        if (ModelState.IsValid)
        {
            var response = await _service.CreateAsync<APIResponse>(model.EntityCreate);
            if (response != null && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (response.ErrorMessages.Count > 0)
                {
                    ModelState.AddModelError("ErrorMessages", response.ErrorMessages.FirstOrDefault());
                }
            }

        }
        var responseList = await _serviceIdentificationType.GetAllAsync<APIResponse>();
        if (responseList != null && responseList.IsSuccess)
        {
            model.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>
                (Convert.ToString(responseList.Result)).Select(x => new SelectListItem
                {
                    Text = x.description,
                    Value = x.registryid.ToString()
                });

            responseList = await _serviceAgent.GetAllAsync<APIResponse>();
            if (responseList != null && responseList.IsSuccess)
            {
                model.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>(
                    Convert.ToString(responseList.Result))
                    .Select(x => new SelectListItem
                    {
                        Text = x.name,
                        Value = x.registryuid.ToString()
                    });
            }
            responseList = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
            if (responseList != null && responseList.IsSuccess)
            {
                model.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>(
                    Convert.ToString(responseList.Result))
                    .Select(x => new SelectListItem
                    {
                        Text = x.name,
                        Value = x.registryuid.ToString()
                    });
            }
            responseList = await _servicesegment.GetAllAsync<APIResponse>();
            if (responseList != null && responseList.IsSuccess)
            {
                model.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(
               Convert.ToString(responseList.Result))
               .Select(x => new SelectListItem
               {
                   Text = x.AssignedSegment,
                   Value = x.IdSegment.ToString()
               });
            }
        }
        return View(model);
    }


    //[HttpGet]
    //public async Task<IActionResult> Upsert(string code) 
    //{
    //    if(code == null)
    //    {
    //        var model = new CustomerCreateVM();

    //        var responseList = await _serviceIdentificationType.GetAllAsync<APIResponse>();

    //        if ((responseList != null) && responseList.IsSuccess)
    //        {
    //            model.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>(
    //                Convert.ToString(responseList.Result))
    //                .Select(x => new SelectListItem
    //                {
    //                    Text = x.description,
    //                    Value = x.registryid.ToString(),
    //                });
    //        }

    //        responseList =await _serviceAgent.GetAllAsync<APIResponse>();
    //        if (responseList != null && responseList.IsSuccess)
    //        {
    //            model.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>(
    //                Convert.ToString(responseList.Result))
    //                .Select(x => new SelectListItem
    //                {
    //                    Text = x.name,
    //                    Value = x.registryuid.ToString(),
    //                });
    //        }

    //        responseList = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
    //        if (responseList != null && responseList.IsSuccess)
    //        {
    //            model.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>(
    //                Convert.ToString(responseList.Result))
    //                .Select(x => new SelectListItem
    //                {
    //                    Text = x.name,
    //                    Value = x.registryuid.ToString(),
    //                });
    //            responseList = await _servicesegment.GetAllAsync<APIResponse>();
    //            if (responseList != null && responseList.IsSuccess)
    //            {
    //                model.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(
    //                    Convert.ToString(responseList.Result))
    //                    .Select(x => new SelectListItem
    //                    {
    //                        Text = x.AssignedSegment,
    //                        Value = x.IdSegment.ToString()
    //                    });
    //            }
    //        }

    //        return View(model);
    //    }
    //    return View(Index);
    //}


    [HttpGet]
    public async Task<IActionResult> UpdateCustomer(string code )
    {
        CustomerUpdateVM customerUpdateVM = new();
        var response = await _service.GetByCodeAsync<APIResponse>(code);
        if (response != null && response.IsSuccess)
        {
            CustomerDto model = JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(response.Result));
            customerUpdateVM.EntityUpdate = _mapper.Map<CustomerDtoUpdate>(model);
        }
        response = await _serviceIdentificationType.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerUpdateVM.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>
                    (Convert.ToString(response.Result)).Select(x => new SelectListItem
                    {
                        Text = x.description,
                        Value = x.registryid.ToString()
                    });
            response = await _serviceAgent.GetAllAsync<APIResponse>();
            if (response != null && response.IsSuccess)
            {
                customerUpdateVM.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>
               (Convert.ToString(response.Result)).Select(x => new SelectListItem
               {
                   Text = x.name,
                   Value = x.registryuid.ToString()
               });
            }

            response = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
            if (response != null && response.IsSuccess)
            {
                customerUpdateVM.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>
               (Convert.ToString(response.Result)).Select(x => new SelectListItem
               {
                   Text = x.name,
                   Value = x.registryuid.ToString()
               });
            }

            response = await _servicesegment.GetAllAsync<APIResponse>();
            if (response != null && response.IsSuccess)
            {
                customerUpdateVM.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(
               Convert.ToString(response.Result))
               .Select(x => new SelectListItem
               {
                   Text = x.AssignedSegment,
                   Value = x.IdSegment.ToString()
               });
                
            }
            return View(customerUpdateVM);
        }
        return NotFound();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateCustomer(string code,CustomerUpdateVM model)
    {
        if (ModelState.IsValid)
        {
            var response = await _service.UpdateAsync<APIResponse>(code, model.EntityUpdate);
            if ((response != null) && response.IsSuccess)
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                if (response.ErrorMessages.Count > 0)
                {
                    ModelState.AddModelError("ErrorMessages", response.ErrorMessages.FirstOrDefault());
                }
            }
        }
        var list = await _servicesegment.GetAllAsync<APIResponse>();
        if (list != null && list.IsSuccess)
        {
            model.SegmentoList= JsonConvert.DeserializeObject<List<SegmentDto>>
                (Convert.ToString(list.Result)).Select(x => new SelectListItem
                {
                    Text = x.AssignedSegment,
                    Value = x.IdSegment.ToString()
                }); 

            list = await _serviceAgent.GetAllAsync<APIResponse>();
            if (list != null && list.IsSuccess)
            {
                model.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>
                    (Convert.ToString(list.Result)).Select(x => new SelectListItem
                    {
                        Text = x.name,
                        Value = x.registryuid.ToString()
                    });
                list = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
                if(list != null && list.IsSuccess)
                {
                    model.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>
                        (Convert.ToString(list.Result)).Select(x => new SelectListItem
                        {
                            Text = x.name,
                            Value = x.registryuid.ToString()
                        });
                    list = await _serviceIdentificationType.GetAllAsync<APIResponse>();
                    if (list != null && list.IsSuccess)
                    {
                        model.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>
                            (Convert.ToString(list.Result)).Select(x => new SelectListItem
                            {

                            });
                    }

                }
            }
        }
       
            return View(model);
    }
    //CREATE
    //GET => BLANCO. Se llega desde el INDEX
    //POST => GUARDAR

    [HttpGet]
    public async Task<IActionResult> DeleteCustomer(string code)
    {
        CustomerDeleteVM customerDeleteVM = new();
        var response = await _service.GetByCodeAsync<APIResponse>(code);
        if (response != null && response.IsSuccess)
        {
            CustomerDto model =JsonConvert.DeserializeObject<CustomerDto>(Convert.ToString(response.Result));
            customerDeleteVM.EntityDelete = model;
            

        }
        response = await _serviceAgent.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDeleteVM.EjecutivoVentaList = JsonConvert.DeserializeObject<List<AgentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
        }
        response = await _serviceCollectionAgent.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDeleteVM.EjecutivoCobroList = JsonConvert.DeserializeObject<List<CollectionAgentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.registryuid.ToString()
                });
        }

        response = await _serviceIdentificationType.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDeleteVM.TipoIdentList = JsonConvert.DeserializeObject<List<CustomerIdentiDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.description,
                    Value = x.registryid.ToString()
                });
        }

        response = await _servicesegment.GetAllAsync<APIResponse>();
        if (response != null && response.IsSuccess)
        {
            customerDeleteVM.SegmentoList = JsonConvert.DeserializeObject<List<SegmentDto>>(Convert.ToString(response.Result)).
                Select(x => new SelectListItem
                {
                    Text = x.AssignedSegment,
                    Value = x.IdSegment.ToString()
                });
                return View(customerDeleteVM);
        }
        return NotFound();
    }


    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteCustomer(CustomerDeleteVM model)
    {

       var response = await _service.DeleteAsync<APIResponse>(model.EntityDelete.Code);
        
        if (response != null && response.IsSuccess)
        {
            TempData["success"] = "Cliente eliminado";
            return RedirectToAction(nameof(Index));
        }
        else
        {    
            return Json (new { response.ErrorMessages });
        }

        return View(model);
    }
    
}
