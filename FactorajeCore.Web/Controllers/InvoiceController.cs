﻿using FactorajeCore.Web.Models;
using FactorajeCore.Web.Models.Dtos;
using FactorajeCore.Web.Models.VM;
using FactorajeCore.Web.ServicesApi.IServices;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Security.Cryptography.Xml;
using System.Threading.Tasks;
using System.Xml;

namespace FactorajeCore.Web.Controllers;

public class InvoiceController : Controller
{
    private readonly ILetraDeCorteService _letraDeCorteService;
    private readonly ICustomerService _customerService;
    private readonly IInvoicesServices _invoicesServices;
   
   
    public InvoiceController(ILetraDeCorteService letraDeCorteService, IInvoicesServices invoicesServices, ICustomerService customerService)
    {
        _letraDeCorteService = letraDeCorteService;
        _customerService = customerService;
        _invoicesServices = invoicesServices;

    }
    public async Task<IActionResult> Index()
    {
        return View();
    }
    [HttpPost]
    public async Task<IActionResult> GeneraFac(DateTime fechaFin)
    {
        List<InvoicesDto> listInvoice = new();
        List<LetraCorteDto> list = new();
        var fechaFormateadaini = new DateTime(fechaFin.Year, fechaFin.Month, 01).Date;
        var fechaIni  = fechaFormateadaini.ToString("yyyy-MM-dd");

        var response = await _letraDeCorteService.GetByDateAsync<APIResponse>(fechaIni.ToString(), fechaFin.ToString("yyyy-MM-dd"));
        if ((response != null) && response.IsSuccess)
        {
            list = JsonConvert.DeserializeObject<List<LetraCorteDto>>(Convert.ToString(response.Result)).ToList();
            var resultadoSum = (from item in list
                                group item by new { item.clientecodigo, item.monedaletra, item.clientenombre, item.clienteCed, item.typeidenti, item.Email } into itemCliente
                                select new
                                {
                                    Email = itemCliente.Key.Email,
                                    typeidenti = itemCliente.Key.typeidenti,
                                    clienteCed = itemCliente.Key.clienteCed,
                                    clienteNombre = itemCliente.Key.clientenombre,
                                    currencyCode = itemCliente.Key.monedaletra,
                                    clienteCodigo = itemCliente.Key.clientecodigo,
                                    interescorrientesum = itemCliente.Sum(x => x.interescorrientedevengadocorte),
                                    interesvencidosum = itemCliente.Sum(x => x.interesvencidodevengadocorte),
                                    interespenalsum = itemCliente.Sum(x => x.interespenaldevengadocorte)
                                }).ToList();

            //Obtener facturas electronicas generadas
            int lastInternalNumber = 0;
            List<InvoicesDto> invoicesList = new();
            var invoicesExist = await _invoicesServices.GetAllDateAsync<APIResponse>(fechaFin.ToString("yyyy-MM-dd")); //GetAllByDateAsync<APIResponse>(fechaFin.ToString("yyyy-MM-dd"));
            if (invoicesExist.IsSuccess)
            {
                invoicesList = JsonConvert.DeserializeObject<List<InvoicesDto>>(Convert.ToString(invoicesExist.Result));
                if (invoicesList.Count > 0)
                {
                    lastInternalNumber = invoicesList.Max(x => x.referenceinternalnumber);
                    lastInternalNumber++;
                }
            }
            if (lastInternalNumber == 0)
            {
                lastInternalNumber = 1;
            }


            var createAt = DateTime.Now;
            string createAtString = createAt.ToString("O");
            createAtString = createAtString.Substring(0, createAtString.Length - 6);
            ViewBag.dateCreateAt = createAtString;
            foreach (var itemTotal in resultadoSum)
            {
                bool indSave = false;
                // si factura existe y no esta cargada
                var invoiceExist = invoicesList.FirstOrDefault(x => x.customercode.Trim().ToUpper() == itemTotal.clienteCodigo.Trim().ToUpper());

                if ((invoiceExist != null) && !invoiceExist.indupload)
                {
                    var deleteInvoice = await _invoicesServices.DeleteAsync<APIResponse>(invoiceExist.registryid);
                    indSave = true;
                }
                else if (invoiceExist == null)
                {
                    indSave = true;
                }

                if (indSave) {
                    var totalinteres = itemTotal.interescorrientesum + itemTotal.interesvencidosum + itemTotal.interespenalsum;


                    var invoiceObject = new InvoicesDtoCreate
                    {

                        invoiceamout = totalinteres,
                        customercode = itemTotal.clienteCodigo,
                        customername = itemTotal.clienteNombre,
                        currencycode = itemTotal.currencyCode,
                        transadate = fechaFin,
                        datajsoninvoice = string.Empty,
                        email = itemTotal.Email,
                        typeidenti = (int)itemTotal.typeidenti,
                        indupload = false,
                        referenceinternalnumber = lastInternalNumber,
                        customeridenti = itemTotal.clienteCed,
                        createdAt= createAt
                    };

                    var saveResult = await _invoicesServices.CreateAsync<APIResponse>(invoiceObject);
                     lastInternalNumber++;

                }


            }

        }
        return View();
    }

    [HttpGet]
    public async Task<IActionResult> MostrarFac(DateTime dateCreateAt)
    {
        string createAtString = dateCreateAt.ToString("O");
        createAtString = createAtString.Substring(0, createAtString.Length - 1);

        InvoiceIndexVM invoice = new();

        List<InvoicesDto> listInvoice = new();
        var task = await _invoicesServices.GetAllCreatedAtAsync<APIResponse>(createAtString);
              
        if ((task != null) && task.IsSuccess)
        {
            listInvoice = JsonConvert.DeserializeObject<List<InvoicesDto>>(Convert.ToString(task.Result)).ToList();
        }
        var listViewData = listInvoice.Select(x => new InvoiceIndexVM
        {
                indload = x.indupload,
                referenceinternalnumber = x.referenceinternalnumber,
                codigo = x.customercode,
                nombre = x.customername,
                monto = x.invoiceamout,
                numeroFac = x.invoicenumber

        }).ToList();

         return Json(new { data = listViewData });           
    }

    [HttpGet]
    public async Task<IActionResult> Refrescar(DateTime fechaFin)
    {
       
        InvoiceIndexVM invoice = new();
        List<InvoicesDto> listInvoice = new();
        var task = await _invoicesServices.GetAllDateAsync<APIResponse>(fechaFin.ToString("yyyy-MM-dd"));

        if ((task != null) && task.IsSuccess)
        {
            listInvoice = JsonConvert.DeserializeObject<List<InvoicesDto>>(Convert.ToString(task.Result)).ToList();
        }

        var listViewData = listInvoice.Select(x => new InvoiceIndexVM
        {
            indload = x.indupload,
            referenceinternalnumber = x.referenceinternalnumber,
            codigo = x.customercode,
            nombre = x.customername,
            monto = x.invoiceamout,
            numeroFac = x.invoicenumber

        }).ToList();

        return Json(new { data = listViewData });

    }
}


