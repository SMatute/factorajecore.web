﻿using AutoMapper;

using FactorajeCore.Web.Models.Dtos;

namespace FactorajeCore.Web
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            //Tsource, TDestination
            CreateMap<CurrenciesDto, CurrenciesDtoUpdate>().ReverseMap();
            CreateMap<ExchangeRateDto, ExchangeRateDtoCreate>().ReverseMap();
            CreateMap<ExchangeRateDto, ExchangeRateDtoUpdate>().ReverseMap();
            CreateMap<CustomerDto, CustomerDtoCreate>().ReverseMap();
            CreateMap<CustomerDto, CustomerDtoUpdate>().ReverseMap();
        }
    }
}
