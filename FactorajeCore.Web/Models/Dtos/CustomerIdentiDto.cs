﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public class CustomerIdentiDto
{
    public int registryid { get; set; }
    public int registrynumber { get; set; }
    public string description { get; set; } = null!;
}
