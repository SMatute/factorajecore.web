﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public class CurrenciesDtoUpdate
{
    [Display(Name = "Código")]
    public string Code { get; set; } = null!;
    [Display(Name = "Descripción")]
    public string Description { get; set; } = null!;
    [Display(Name = "Número")]
    public string Number { get; set; } = null!;
    [Display(Name = "Símbolo")]
    public string Symbol { get; set; } = null!;
}
