﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public class SegmentDto
{
    public int IdSegment { get; set; }
    public string AssignedSegment { get; set; } = null!;
    public string SegmentInit { get; set; } = null!;
    public DateTime CreateDate { get; set; }
    public DateTime ModDate { get; set; }
    public string CreatUser { get; set; } = null!;
    public string ModUser { get; set; } = null!;
    public byte flgdef { get; set; }
}
