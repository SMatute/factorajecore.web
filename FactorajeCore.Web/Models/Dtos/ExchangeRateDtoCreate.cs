﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FactorajeCore.Web.Models.Dtos;

public class ExchangeRateDtoCreate
{
    [Required(ErrorMessage = "El Codigo de la moneda es requerido")]
    [StringLength(3)]
    [Display(Name = "Moneda")]
    public string currencyCode { get; set; } = null!;

    [Required(ErrorMessage = "La fecha es requerido")]
    [StringLength(10)]
    [Display(Name = "Fecha")]
    public DateTime dateTrx { get; set; }
    [Required(ErrorMessage = "Tipo de cambio es requerido")]
    [Display(Name = "Tipo de cambio")]
    public decimal? officialRate { get; set; }
}
