﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public class CustomerDtoUpdate
{

    [Required(ErrorMessage = "El Codigo del cliente es requerido")]
    [StringLength(10)]
    [Display(Name = "Código")]
    public string Code { get; set; } = null!;

    [Required(ErrorMessage = "Razón Social es requerido")]
    [StringLength(50)]
    [Display(Name = "Razón Social")]
    public string Razon { get; set; } = null!;

    [Required(ErrorMessage = "Nombre Comercial es requerido")]
    [StringLength(50)]
    [Display(Name = "Nom. Comercial")]

    public string? Name { get; set; }
    [Required(ErrorMessage = "Dirección es requerido")]
    [StringLength(255)]
    [Display(Name = "Dirección")]
    public string? Address { get; set; }

    [Required(ErrorMessage = "Teléfono es requerido")]
    [StringLength(12)]
    [Display(Name = "Teléfonos")]
    public string? Phone1 { get; set; }

    [StringLength(12)]
    [Display(Name = "Celular")]
    public string? Phone2 { get; set; }

    [StringLength(12)]
    [Display(Name = "Fax")]
    public string? Fax1 { get; set; } = null!;

    [Display(Name = "Estado")]
    public string? Active { get; set; } = null!;

    public string? TypeCustomer { get; set; }

    public string? CLIACT { get; set; }

    [Required(ErrorMessage = "Fecha es requerido")]
    [Display(Name = "Fecha de ingreso")]
    public DateTime Dateadmission { get; set; }

    [Required(ErrorMessage = "Limite de credito es requerido")]
    [Display(Name = "Limite de credito")]
    public decimal CreditLimit { get; set; }

    [Required(ErrorMessage = "Principal Saldo es requerido")]
    [Display(Name = "Principal Saldo")]
    public decimal PrincipalBalance { get; set; }

    public decimal BalanceInterest { get; set; }

    [Required(ErrorMessage = "Principal Vencido es requerido")]
    [Display(Name = "Principal Vencido")]
    public decimal ArrearsBalance { get; set; }

    public decimal Normalinterest { get; set; }

    [Required(ErrorMessage = "Interés Corriente es requerido")]
    [Display(Name = "Interés Corriente")]
    public decimal CurrentInterest { get; set; }

    [Required(ErrorMessage = "Interés Moratorio es requerido")]
    [Display(Name = "Interés Moratorio")]
    public decimal MoratoriumInterest { get; set; }

    [Required(ErrorMessage = "Observación es requerido")]
    [StringLength(100)]
    [Display(Name = "Observación")]
    public string? Observations { get; set; }

    public int CLIFOT { get; set; }

    [StringLength(25)]
    [Display(Name = "Número de cédula")]
    public string? NumberCED { get; set; } = null!;

    [StringLength(25)]
    [Display(Name = "Número RUC")]
    public string? NumberRuc { get; set; } = null!;

    public int IdTypeCustomer { get; set; }

    [Required(ErrorMessage = "Agente es requerido")]
    [Display(Name = "Agente de venta")]
    public int IdAgent { get; set; }

    //[Required(ErrorMessage = "Sector es requerido")]
    [Display(Name = "Sector")]
    public int? IdSegment { get; set; } = null!;

    public string? BusinessId { get; set; }

    public string? DIRELE { get; set; }

    [Required(ErrorMessage = "Agente es requerido")]
    [Display(Name = "Agente de cobro")]
    public int? IdCollectionAgent { get; set; }

    [Required(ErrorMessage = "Correo electronico es requerido")]
    [StringLength(75)]
    [Display(Name = "Email")]
    public string? Email { get; set; }
    [Required(ErrorMessage = "Tipo de Identificación es requerido")]
    [Display(Name = "T/Identificación")]
    public int Type { get; set; }


}

