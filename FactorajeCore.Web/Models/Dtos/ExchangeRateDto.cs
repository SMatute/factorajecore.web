﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public  class ExchangeRateDto
{
    public string currencyCode { get; set; } = null!;
    public DateTime dateTrx { get; set; }
    public decimal? officialRate { get; set; }
}
