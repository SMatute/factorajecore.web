﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;
public class LetraCorteDto
{
    public int numeroletra { get; set; }
    public DateTime fechacorte { get; set; }
    public DateTime fechacorteanterior { get; set; }
    public short diascorrientescorte { get; set; }
    public short diasvencidoscorte { get; set; }
    public int? typeidenti { get; set; }
    public string Email { get; set; }
    public string clienteCed { get; set; }
    public string? clientecodigo { get; set; }
    public string? clientenombre { get; set; }
    public string? monedaletra { get; set; }
    public decimal interescorrientedevengadocorte { get; set; }
    public decimal interesvencidodevengadocorte { get; set; }
    public decimal interespenaldevengadocorte { get; set; }
    public decimal interescorrienteabonocorte { get; set; }
    public decimal interesvencidoabonocorte { get; set; }
    public decimal interespenalabonocorte { get; set; }




}
