﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorajeCore.Web.Models.Dtos;

public class CollectionAgentDto
{
    public int registryuid { get; set; }
    public string name { get; set; } = null!;
    public string last { get; set; } = null!;
    public string abrevation { get; set; } = null!;
    public DateTime createdate { get; set; }
    public DateTime updatedate { get; set; }
    public string createuser { get; set; } = null!;
    public string? updateuser { get; set; }
    public bool defaultindicator { get; set; }
}
