﻿namespace FactorajeCore.Web.Models.Dtos
{
    public class InvoicesDtoUpdate
    {
        public int registryid { get; set; }
        public string customercode { get; set; }
        public string customername { get; set; }
        public string currencycode { get; set; }
        public int? invoicenumber { get; set; }
        public decimal invoiceamout { get; set; }
        public string email { get; set; }
        public string customeridenti { get; set; }
        public int typeidenti { get; set; }
        public string? datajsoninvoice { get; set; }
        public bool indupload { get; set; }
        public int referenceinternalnumber { get; set; }
        public DateTime transadate { get; set; }
        public DateTime createdAt { get; set; }

    }
}
