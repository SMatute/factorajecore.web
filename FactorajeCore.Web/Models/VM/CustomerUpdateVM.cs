﻿using FactorajeCore.Web.Models.Dtos;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FactorajeCore.Web.Models.VM
{
    public class CustomerUpdateVM
    {
        public CustomerUpdateVM()
        {
            EntityUpdate = new();
        }
        public CustomerDtoUpdate EntityUpdate { get; set; }
        [ValidateNever]
        public IEnumerable<SelectListItem> TipoIdentList { get; set; }
        [ValidateNever]
        public IEnumerable<SelectListItem> EjecutivoVentaList { get; set; }
        [ValidateNever]
        public IEnumerable<SelectListItem> EjecutivoCobroList { get; set; }
        [ValidateNever]
        public IEnumerable<SelectListItem> SegmentoList { get; set; }
    }
}
