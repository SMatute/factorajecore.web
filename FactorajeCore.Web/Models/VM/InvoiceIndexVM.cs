﻿using FactorajeCore.Web.Models.Dtos;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FactorajeCore.Web.Models.VM;

public class InvoiceIndexVM
{
    public bool indload { get; set; }
    public int referenceinternalnumber { get; set; }
    public string codigo { get; set; } = null!;
    public string nombre { get; set; } = null!;
    public decimal? monto { get; set; }
    public int? numeroFac { get; set;}
}
   
