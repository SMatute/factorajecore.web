﻿using FactorajeCore.Web.Models.Dtos;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FactorajeCore.Web.Models.VM;

public class CustomerDeleteVM
{
    public CustomerDeleteVM()
    {
        EntityDelete = new CustomerDto();
    }
    public CustomerDto EntityDelete { get; set; }

    [ValidateNever]
    public IEnumerable<SelectListItem>? TipoIdentList { get; set; }
    [ValidateNever]
    public IEnumerable<SelectListItem>? EjecutivoVentaList { get; set; }
    [ValidateNever]
    public IEnumerable<SelectListItem>? EjecutivoCobroList { get; set; }
    public IEnumerable<SelectListItem>? SegmentoList { get; set; }
}
