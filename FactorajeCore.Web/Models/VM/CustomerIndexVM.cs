﻿using System.ComponentModel.DataAnnotations;

namespace FactorajeCore.Web.Models.VM
{
    public class CustomerIndexVM
    {
        [Key]
        public string code { get; set; } 
        public string razon { get; set; } = null!;
        public string numberidentity { get; set; } = null!;
        public string? email { get; set; }

    }
}
