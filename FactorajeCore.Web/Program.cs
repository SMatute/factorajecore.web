using FactorajeCore.Web;
using FactorajeCore.Web.ServicesApi.IServices;
using FactorajeCore.Web.ServicesApi.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();
builder.Services.AddAutoMapper(typeof(MappingProfile));

builder.Services.AddHttpClient<ICurrencyService, CurrencyService>();
builder.Services.AddScoped<ICurrencyService, CurrencyService>();
builder.Services.AddScoped<ICustomerService, CustomerService>();
builder.Services.AddScoped<ICustomerIdentiService, CustomerIdentiService>();
builder.Services.AddScoped<IAgentService, AgentService>();
builder.Services.AddScoped<ICollectionAgentService, CollectionAgentService>();
builder.Services.AddScoped<ISegmentService, SegmentService>();
builder.Services.AddScoped<IExchangeRateServices, ExchangeRateServices>();
builder.Services.AddScoped<IInvoicesServices, InvoicesServices>();
builder.Services.AddScoped<ILetraDeCorteService, LetraDeCorteService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
